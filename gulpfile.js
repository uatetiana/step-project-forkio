const { src, dest } = require('gulp');
const gulp = require('gulp');
const syncBrowser = require('browser-sync').create();
const scss = require('gulp-sass');
const destruction = require('del');
const autoprefixer = require('gulp-autoprefixer');
const cssClean = require('gulp-clean-css');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify-es').default;
const woff = require('gulp-ttf2woff');
const groupMedia = require('gulp-group-css-media-queries');

const projectFolder = "dist";
const sourceFolder = "src";

const path = {
	build: {
		html: projectFolder,
		css: projectFolder + "/css/",
		javascript: projectFolder + "/javascript/",
		img: projectFolder + "/img/",
		icons: projectFolder + "/icons",
		fonts: projectFolder + "/fonts/",
	},
	src: {
		html: "./index.html",
		css: sourceFolder + "/scss/style.scss",
		javascript: sourceFolder + "/javascript/index.js",
		img: sourceFolder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
		icons: sourceFolder + "/icons/**/*.{jpg,png,svg,gif,ico,webp}",
		fonts: sourceFolder + "/fonts/**/*.ttf",
	},
	watch: {
		html: sourceFolder + "/**/*.html",
		css: sourceFolder + "/scss/**/*.scss",
		javascript: sourceFolder + "/javascript/**/*.js",
		img: sourceFolder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
	},
	cleanFolder: "./" + projectFolder + "/",
}

function browserSync() {
	syncBrowser.init({
		server: {
			baseDir: "./" + projectFolder + "/"
		},
		port: 5500,
		notify: false
	})
}

function html() {
	return src(path.src.html)
		.pipe(dest(path.build.html))
		.pipe(syncBrowser.stream())
}

function css() {
	return src(path.src.css)
		.pipe(scss({ outputStyle: "expanded" }))
		.pipe(groupMedia())
		.pipe(autoprefixer({
			overrideBrowserslist: ["last 5 versions"],
			cascade: true
		}))
		.pipe(cssClean())
		.pipe(rename({ suffix: ".min" }))
		.pipe(dest(path.build.css))
		.pipe(syncBrowser.stream())
}

function javaScript() {
	return src(path.src.javascript)
		.pipe(uglify())
		.pipe(rename({ extname: ".min.js" }))
		.pipe(dest(path.build.javascript))
		.pipe(syncBrowser.stream())
}

function fonts() {
	return gulp.src(path.src.fonts)
		.pipe(woff())
		.pipe(gulp.dest(path.build.fonts))
}

function img() {
	return src(path.src.img)
		.pipe(dest(path.build.img))
		.pipe(syncBrowser.stream())
}

function icons() {
	return src(path.src.icons)
		.pipe(dest(path.build.icons))
		.pipe(syncBrowser.stream())
}

function watchFiles() {
	gulp.watch([path.watch.html], html);
	gulp.watch([path.watch.css], css);
	gulp.watch([path.watch.javascript], javaScript);
	gulp.watch([path.watch.img], img);
}

function clean() {
	return destruction(path.cleanFolder);
}

let build = gulp.series(clean, gulp.parallel(javaScript, css, html, fonts, img, icons));
let dev = gulp.parallel(build, watchFiles, browserSync);

exports.build = build;
exports.dev = dev;
